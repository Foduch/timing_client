import requests
import logging

import settings


class API():
    base_uri = settings.API_URI
    race_id = settings.RACE_GUID

    def __init__(self,):
        self.logger = logging.getLogger('api')
        self.logger.setLevel(logging.DEBUG)
        fh = logging.FileHandler('api.log')
        fh.setLevel(logging.DEBUG)
        self.logger.addHandler(fh)

    def log(self, message):
        self.logger.info(message)

    def send_point(self, point):
        self.log('sending: tag {0} time {1}'.format(point['tag'], point['time']))
        try:
            response = requests.post(
                self.base_uri + '/race/{0}/point'.format(self.race_id),
                data={'tag': point['tag'], 'date_time': point['time']}
            )
        except:
            self.log('ERROR')
